#define _GNU_SOURCE
#include <stdio.h>
#include <msgpack.h>
#include <stdlib.h>
#include <stdarg.h>

#include "utils.h"

int verbose;

int strcnt(const char* haystack, const char needle) {
    int cnt = 0;
    while(*haystack != 0) {
        if(*haystack == needle)
            cnt++;
        haystack++;
    }
    return cnt;
}

const char* to_v6_subnet(const char* ip) {
    static char buffer[45]; // Enough to hold IPv6/64 + 0x00 + few spare bytes
    if(strchr(ip,'/') != NULL)
        return ip;
    int cnt = 0, rem = 0, j = 0, i = 0;
    for(i = 0, j = 0; j<45 && ip[i] != 0 && cnt < 4; j++, i++) {
        if(ip[i] == ':') {
            // Expand ::
            if(i > 0 && ip[i-1] == ':') {
                // Are we ending anyway?
                if(cnt == 3) {
                    j--;
                    break;
                }
                // How many many : do we have left?
                rem = strcnt(ip+i+1,':');
                // We want to strip last three
                if(rem <= 3) {
                    j--;
                    break;
                }
                // We need to fill '0:' till we get to continue
                while(cnt + rem < 7) {
                    buffer[j] = ':';
                    j++;
                    buffer[j] = '0';
                    j++;
                    cnt++;
                }
            }
            cnt++;
            if(cnt == 4) {
                break;
            }
        }
        buffer[j] = ip[i];
    }
    buffer[j] = 0;
    strncpy(buffer + j, "::/64", 45 - j);
    return buffer;
}

bool is_ipv6(const char* addr) {
    if(strchr(addr, ':') != NULL) {
        return true;
    } else {
        return false;
    }
}

int systemf(const char *fmt, ...) {
    va_list ap;
    int ret = 0;
    char *buff = NULL;

    // Create a buffer, construct command and run it
    va_start(ap, fmt);
    vasprintf(&buff, fmt, ap);
    assert(buff != NULL);
    ret = system(buff);
    va_end(ap);
    free(buff);
    return ret;
}

void set_msgpack_string(char *buff, size_t len, msgpack_object_str *msg) {
    size_t sz = (msg->size + 1) > len ? len : msg->size + 1;
    snprintf(buff, sz, "%s", msg->ptr);
    buff[sz] = 0;
}
