#include <czmq.h>
#include <zsys.h>
#include <ctype.h>
#include <msgpack.h>

#include "client.h"
#include "utils.h"

client::client(const char* endpoint, const char* certpath) {
    //  Create and start authentication engine
    auth = zactor_new(zauth, NULL);
    zstr_sendx(auth,"VERBOSE", NULL);
    zsock_wait(auth);
    zstr_sendx(auth,"ALLOW", "0.0.0.0", NULL);
    zsock_wait(auth);
 
    //  Tell the authenticator how to handle CURVE requests
    zstr_sendx(auth,"CURVE", CURVE_ALLOW_ANY, NULL);
    zsock_wait(auth);

    //  We need two certificates, one for the client and one for
    //  the server. The client must know the server's public key
    //  to make a CURVE connection.
    client_cert = zcert_new ();
    server_cert = zcert_load(certpath);
    const char *server_key = zcert_public_txt(server_cert);
        
    //  Create and connect client socket
    sock = zsock_new(ZMQ_SUB);
    //  Use monitor to monitor the connection (and get debug output)
    clientmon = zactor_new(zmonitor, sock);
    zstr_sendx(clientmon, "VERBOSE", NULL);
    zstr_sendx(clientmon, "LISTEN", "ALL", NULL);
    zstr_sendx(clientmon, "START", NULL);
    zcert_apply(client_cert, sock);
    zsock_set_curve_serverkey(sock, server_key);
    zsock_set_subscribe(sock, "dynfw/list");
    zsock_connect(sock, "%s", endpoint);
}

client::~client() {
    //  Cleanup
    zactor_destroy(&clientmon);
    zcert_destroy(&client_cert);
    zcert_destroy(&server_cert);
    zactor_destroy(&auth);
    zsock_destroy(&sock);
}

//! Checks whether string contains just something like IP address
bool check_ip(const char *ip) {
    size_t i;
    for(i = 0; i < strlen(ip); i++) {
        if(ip[i] != '.' && ip[i] != ':' && ip[i] != '/' && !isxdigit(ip[i])) {
            zsys_warning("Received non-ip address '%s'", ip);
            return false;
        }
    }
    if(i<7) {
        zsys_warning("Received IP address '%s' with length %u - too short to be an IP", ip, strlen(ip));
        return false;
    }
    return true;
}

int client::process_message(bad_guy_manager* bad) {
    // We need to store serial and version to see when we missed messages
    static uint64_t sserial = 0;

    // We also need to know what return code to return
    int ret = 0;

    // Get a message
    zmsg_t *msg = zmsg_recv(sock);
    if(msg == NULL)
        return -1;

    // Let's disassemble the message
    char *topic = zmsg_popstr(msg);
    zframe_t *frame = zmsg_pop(msg);
    zmsg_destroy(&msg);

    // Check that we got topic and content
    if(topic == NULL || frame == NULL) {
        if(topic)
            free(topic);
        if(frame)
            zframe_destroy(&frame);
        zsys_error("No topic or content\n");
        return -1;
    }

    // Prepare local buffers
    char buff[1024];
    char delta[128];
    char ip[256];
    buff[0] = ip[0] = delta[0] = 0;
    uint64_t serial = 0;
    msgpack_object_array *list = NULL;
    uint64_t version = 0;
    msgpack_object root;

    // Unpack msgpack
    msgpack_unpacked msgp;
    msgpack_unpacked_init(&msgp);
    if(!msgpack_unpack_next(&msgp, (const char*)zframe_data(frame), zframe_size(frame), NULL)) {
        zsys_error("Unpacking failed\n");
        ret = -1;
        goto process_message_msg_err;
    }

    // Prepare for msgpack parsing
    root = msgp.data;
    // msgpack_object_print(stdout,root); printf("\n");
    if(root.type != MSGPACK_OBJECT_MAP) {
        zsys_error("Wrong message\n");
        ret = -1;
        goto process_message_msg_err;
    }

    // Walk trough msgpack structure and store locally interesting values
    for(unsigned int i=0; i<root.via.map.size; i++) {
        if(root.via.map.ptr[i].key.type == MSGPACK_OBJECT_STR) {
            set_msgpack_string(buff, 1023, &root.via.map.ptr[i].key.via.str);
            if(strcmp(buff,"serial") == 0 && root.via.map.ptr[i].val.type == MSGPACK_OBJECT_POSITIVE_INTEGER) {
                serial = root.via.map.ptr[i].val.via.u64;
            } else if(strcmp(buff,"ip") == 0 && root.via.map.ptr[i].val.type == MSGPACK_OBJECT_STR) {
                set_msgpack_string(ip, 255, &root.via.map.ptr[i].val.via.str);
            } else if(strcmp(buff,"delta") == 0 && root.via.map.ptr[i].val.type == MSGPACK_OBJECT_STR) {
                set_msgpack_string(delta, 128, &root.via.map.ptr[i].val.via.str);
            } else if(strcmp(buff,"list") == 0 && root.via.map.ptr[i].val.type == MSGPACK_OBJECT_ARRAY) {
                list = &root.via.map.ptr[i].val.via.array;
            } else if(strcmp(buff,"version") == 0 && root.via.map.ptr[i].val.type == MSGPACK_OBJECT_POSITIVE_INTEGER) {
                version = root.via.map.ptr[i].val.via.u64;
            }
        }
    }

    // Handle list messgae
    if(strcmp(topic,"dynfw/list") == 0 && list != NULL) {
        zsys_info("Received new list with serial %u containing %u ips", serial, list->size);
        // Unsubscribe from list and subscribe to deltas
        zsock_set_subscribe(sock, "dynfw/delta");
        zsock_set_unsubscribe(sock, "dynfw/list");

        sserial = serial;
        bad->clear();
        for(unsigned int i=0; i<list->size; i++) {
            set_msgpack_string(ip, 255, &(list->ptr[i].via.str));
            if(check_ip(ip)) {
                bad->add_list(ip);
                ret++;
            }
        }
        bad->commit_list();

    // Handle delta message
    } else if(strcmp(topic,"dynfw/delta") == 0) {
        zsys_info("Received list update with serial %u", serial);

        if(strcmp(delta,"positive") == 0 && check_ip(ip)) {
            bad->add(ip);
            ret++;
        }

        if(strcmp(delta,"negative") == 0 && check_ip(ip)) {
            bad->remove(ip);
            ret++;
        }

        // If we missed something, unsubscribe from delta and wait for a list message
        sserial++;
        if(sserial != serial) {
            zsock_set_subscribe(sock, "dynfw/list");
            zsock_set_unsubscribe(sock, "dynfw/delta");
        }
    }

    // Destroy everything
process_message_msg_err:
    msgpack_unpacked_destroy(&msgp);

    if(topic)
        free(topic);
    if(frame)
        zframe_destroy(&frame);
    return ret;
}

