#pragma once

#include "bad_guy_manager.h"

class ipset_manager : public bad_guy_manager {
    private:
        char* ipset;
    public:
        virtual int add(const char *ip);
        virtual int remove(const char *ip);
        virtual int clear();
        ipset_manager(const char* ipset);
        ~ipset_manager();
};

