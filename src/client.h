#pragma once

#include "bad_guy_manager.h"

class client {
    private:
        zactor_t *auth = NULL;
        zcert_t *client_cert = NULL;
        zcert_t *server_cert = NULL;
        zactor_t *clientmon = NULL;
    public:
        zsock_t *sock = NULL;
        client(const char *endpoint, const char *certpath);
        int process_message(bad_guy_manager *bad);
        ~client();
};

