#pragma once

#include <czmq.h>
#include <zsys.h>
#include <msgpack.h>

extern int verbose;

#ifdef __cplusplus
extern "C" {
#endif

bool is_ipv6(const char* ip);
const char* to_v6_subnet(const char* ip);
int systemf(const char *fmt, ...);
void set_msgpack_string(char *buff, size_t len, msgpack_object_str *msg);

#ifdef __cplusplus
}
#endif
