#pragma once

#include "bad_guy_manager.h"
#include <map>
#include <string>

class nfset_manager : public bad_guy_manager {
    private:
        char* table;
        char* set;
        std::map<std::string, int> prefix_counter;
        int prefix_add(const std::string subnet);
        int prefix_del(const std::string subnet);
    public:
        virtual int add(const char *ip);
        virtual int add_list(const char *ip);
        virtual int commit_list();
        virtual int remove(const char *ip);
        virtual int clear();
        nfset_manager(const char* table, const char* set);
        ~nfset_manager();
};

