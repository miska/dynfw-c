#include "ipset_manager.h"
#include "utils.h"

ipset_manager::ipset_manager(const char *set) {
    asprintf(&ipset, "%s", set);
    clear();
}

ipset_manager::~ipset_manager() {
    free(ipset);
}

int ipset_manager::clear() {
    bad_guy_manager::clear();
    systemf("ipset create %s_v4 hash:ip family inet -exist", ipset);
    systemf("ipset create %s_v6 hash:net family inet6 -exist", ipset);
    return systemf("ipset flush %s_v4", ipset) && systemf("ipset flush %s_v6", ipset);
}

int ipset_manager::add(const char *ip) {
    bad_guy_manager::add(ip);
    if(is_ipv6(ip)) {
        return systemf("ipset add %s_v6 %s", ipset, to_v6_subnet(ip));
    } else {
        return systemf("ipset add %s_v4 %s", ipset, ip);
    }
}

int ipset_manager::remove(const char *ip) {
    bad_guy_manager::remove(ip);
    if(is_ipv6(ip)) {
        return systemf("ipset del %s_v6 %s", ipset, to_v6_subnet(ip));
    } else {
        return systemf("ipset del %s_v4 %s", ipset, ip);
    }
}

