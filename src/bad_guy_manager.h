#pragma once

class bad_guy_manager {
    public:
        virtual int add(const char *ip);
        virtual int add_list(const char *ip) {
            return add(ip);
        };
        virtual int commit_list() { return 0; };
        virtual int remove(const char *ip);
        virtual int clear();
};

