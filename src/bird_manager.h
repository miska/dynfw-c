#pragma once

#include <set>
#include <string>

#include "bad_guy_manager.h"

class bird_manager : public bad_guy_manager {
    private:
        std::set<std::string> db;
        std::string tmp_path;
        std::string table;
        time_t r_time;
        time_t last_change = 0;
        int changes = 0;
        int m_changes;
    public:
        virtual int add(const char *ip);
        virtual int remove(const char *ip);
        virtual int clear();
        int snapshot(bool force = false);
        bird_manager(time_t r_time = 10, int changes = 10, const char *table = "turris_dynfw", const char* tmp_path = "/tmp/dynfw_bird_flowspec");
        ~bird_manager();
};

