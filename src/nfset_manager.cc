#include "nfset_manager.h"
#include "utils.h"

#define BUFFLEN 10240
// 4*3 + 3
#define MAXIP4LEN 15
// 8*4 + 7 + 1 + 3
#define MAXIP6LEN 43

char buffer_v4[BUFFLEN];
char buffer_v6[BUFFLEN];
int buffer_v4_len = 0;
int buffer_v6_len = 0;

nfset_manager::nfset_manager(const char *t, const char *s) {
    asprintf(&table, "%s", t);
    asprintf(&set, "%s", s);
    clear();
}

nfset_manager::~nfset_manager() {
    free(set);
    free(table);
}

int nfset_manager::clear() {
    bad_guy_manager::clear();
    int ret = 0;
    systemf("nft create table inet %s \\{ comment \\\"Turris Sentinel DynFW table\\\"\\; \\} 2> /dev/null || true", table);
    systemf("nft flush set inet %s %s_4 2> /dev/null || true", table, set);
    systemf("nft delete set inet %s %s_4 2> /dev/null || true", table, set);
    ret |= systemf("nft add set inet %s %s_4 \\{ type ipv4_addr\\; comment \\\"IPv4 addresses blocked by Turris Sentinel\\\" \\; \\}", table, set);
    systemf("nft flush set inet %s %s_6 2> /dev/null || true", table, set);
    systemf("nft delete set inet %s %s_6 2> /dev/null || true", table, set);
    ret |= systemf("nft add set inet %s %s_6 \\{ type ipv6_addr\\; flags interval\\; comment \\\"IPv6 addresses blocked by Turris Sentinel\\\"\\; \\}", table, set);
    return ret;
}

int nfset_manager::add_list(const char *ip) {
    if(buffer_v4_len == 0) {
        snprintf(buffer_v4, 1024, "nft add element inet %s %s_4 \\{", table, set);
        buffer_v4_len = strlen(buffer_v4);
    }
    if(buffer_v6_len == 0) {
        snprintf(buffer_v6, 1024, "nft add element inet %s %s_6 \\{", table, set);
        buffer_v6_len = strlen(buffer_v6);
    }
    if(is_ipv6(ip)) {
        const char *subnet = to_v6_subnet(ip);
        if(prefix_add(subnet) != 1)
            return 0;
        strncpy(buffer_v6 + buffer_v6_len, subnet, MAXIP6LEN);
        buffer_v6_len += strlen(subnet);
        buffer_v6[buffer_v6_len++] = ',';
        buffer_v6[buffer_v6_len] = 0;
        if((buffer_v6_len + MAXIP6LEN + 3) > BUFFLEN) {
            return commit_list();
        }
    } else {
        strncpy(buffer_v4 + buffer_v4_len, ip, MAXIP4LEN);
        buffer_v4_len += strlen(ip);
        buffer_v4[buffer_v4_len++] = ',';
        buffer_v4[buffer_v4_len] = 0;
        if((buffer_v4_len + MAXIP4LEN + 3) > BUFFLEN) {
            return commit_list();
        }
    }
    return 0;
}

int nfset_manager::commit_list() {
    int ret = 0;
    // Are there any elements in the buffer?
    if(buffer_v4[buffer_v4_len-1] == ',') {
        buffer_v4[buffer_v4_len-1] = '}';
        ret |= system(buffer_v4);
        buffer_v4[0] = 0;
        buffer_v4_len = 0;
    }

    // Are there any elements in the buffer?
    if(buffer_v6[buffer_v6_len-1] == ',') {
        buffer_v6[buffer_v6_len-1] = '}';
        ret |= system(buffer_v6);
        buffer_v6[0] = 0;
        buffer_v6_len = 0;
    }

    return ret;
}

int nfset_manager::prefix_add(const std::string subnet) {
    auto it = prefix_counter.find(subnet);
    if(it == prefix_counter.end()) {
        prefix_counter.emplace(std::pair<std::string,int>(subnet, 1));
        return 1;
    }
    it->second++;
    return it->second;
}

int nfset_manager::prefix_del(const std::string subnet) {
    auto it = prefix_counter.find(subnet);
    if(it == prefix_counter.end()) {
        return -1;
    }
    if(it->second > 1) {
        it->second--;
        return it->second;
    }
    prefix_counter.erase(it);
    return 0;
}

int nfset_manager::add(const char *ip) {
    bad_guy_manager::add(ip);
    if(is_ipv6(ip)) {
        const char *subnet = to_v6_subnet(ip);
        if(prefix_add(subnet) == 1)
            return systemf("nft add element inet %s %s_6 \\{ %s \\}", table, set, subnet);
        return 0;
    } else {
        return systemf("nft add element inet %s %s_4 \\{ %s \\}", table, set, ip);
    }
}

int nfset_manager::remove(const char *ip) {
    bad_guy_manager::remove(ip);
    if(is_ipv6(ip)) {
        const char *subnet = to_v6_subnet(ip);
        if(prefix_del(subnet) <= 0)
            return systemf("nft delete element inet %s %s_6 \\{ %s \\}", table, set, subnet);
        return 0;
    } else {
        return systemf("nft delete element inet %s %s_4 \\{ %s \\}", table, set, ip);
    }
}

