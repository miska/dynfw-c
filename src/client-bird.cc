/**
 * \file client-bird.cc
 *
 * \brief DynFW CLI program to manage a FlowSpec in Bird
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <czmq.h>
#include <zsys.h>
#include <msgpack.h>
#include <getopt.h>
#include <signal.h>

#include "client.h"
#include "utils.h"
#include "bird_manager.h"

const char *table = "turris_dynfw";
const char *tmp_file = "/tmp/dynfw_bird_flowspec";
bird_manager *badguys = NULL;
time_t r_time = 10;
int changes = 10;

void dumper(int signum) {
    zsys_info("Creating a snapshot because signal %d was caught", signal);
    badguys->snapshot(true);
}

void encrypted_loop(const char *endpoint, const char *certpath) {
    zsys_info("Connecting to %s using %s", endpoint, certpath);

    badguys = new bird_manager(r_time, changes, table);
    signal(SIGUSR1, dumper);
    signal(SIGHUP, dumper);
    client cl(endpoint,certpath);

    //  Main loop
    while(zsys_interrupted == 0) {
        cl.process_message(badguys);
    }
}

//! Main body to parse command line arguments
int main(int argc, char **argv) {
    // Setup defaults
    const char *server = "sentinel.turris.cz";
    const char *cert = "/var/run/dynfw/server.cert";
    int port = 7087;

    // Parse options
    struct option long_options[] = {
        {"server",       required_argument, 0,  's'},
        {"port",         required_argument, 0,  'p'},
        {"cert",         required_argument, 0,  'c'},
        {"refresh-time", required_argument, 0,  'r'},
        {"tmp-file",     required_argument, 0,  'f'},
        {"table-name",   required_argument, 0,  't'},
        {"changes",      required_argument, 0,  'x'},
        {"verbose",      no_argument,       0,  'v'},
        {"help",         no_argument,       0,  'h'},
        {0,0,0,0}
    };
    int c;
    int option_index = 0;
    while((c = getopt_long(argc, argv, "s:p:c:r:f:i:t:x:vh", long_options, &option_index)) != -1) {
        switch(c) {
            case 's':
                server = optarg;
                break;
            case 'c':
                cert = optarg;
                break;
            case 't':
                table = strdup(optarg);
                break;
            case 'f':
                tmp_file = strdup(optarg);
                break;
            case 'r':
                r_time = atoi(optarg);
                break;
            case 'x':
                changes = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                printf("Setting verbose mode %d\n", verbose);
                break;
            case 'h':
                printf(
                    "Usage: %s [OPTION...]\n\n"
                    "   -s, --server=SERVER     server to connect to (default %s)\n"
                    "   -p, --port=PORT         port on the server to connect to (default %d)\n"
                    "   -c, --cert=CERT         path to the server certificate (default %s)\n"
                    "   -r, --refresh-time      how often at most regenerate bird config file (default %ds)\n"
                    "   -f, --tmp-file          file to save configuration in (defaul %s)\n"
                    "   -t, --table-name        how to name the resulting protocol in bird (default %s)\n"
                    "   -x, --changes           wait for at least this many changes (default %d)\n"
                    "   -v, --verbose           turn on verbose output\n"
                    "   -h, --help              show help message and exit\n\n"
                    , argv[0], server, port, cert, r_time, tmp_file, table, changes
                );
                exit(0);
        };
    }

    char buff[1024];
    snprintf(buff, 1023, "tcp://%s:%d", server, port);
    encrypted_loop(buff, cert);
    return 0;
}
