#pragma once

#include <set>
#include <string>

#include "bad_guy_manager.h"

class set_manager : public bad_guy_manager {
    private:
        std::set<std::string> db;
        char *cache;
    public:
        virtual int add(const char *ip);
        virtual int remove(const char *ip);
        virtual int clear();
        int snapshot(const char* path);
        set_manager(const char *persistent = "");
        ~set_manager();
};

