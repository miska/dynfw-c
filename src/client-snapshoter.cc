/**
 * \file client.cc
 *
 * \brief DynFW CLI program to manage a designated ipset
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <czmq.h>
#include <zsys.h>
#include <msgpack.h>
#include <getopt.h>
#include <signal.h>

#include "client.h"
#include "utils.h"
#include "set_manager.h"

char *dump_path;
set_manager *badguys;

void dumper(int signum) {
    zsys_info("Creating snapshot in %s", dump_path);
    badguys->snapshot(dump_path);
}

void encrypted_loop(const char *endpoint, const char *certpath) {
    zsys_info("Connecting to %s using %s", endpoint, certpath);

    badguys = new set_manager;
    signal(SIGUSR1, dumper);
    client cl(endpoint,certpath);

    //  Main loop
    while(zsys_interrupted == 0) {
        cl.process_message(badguys);
    }
}

//! Main body to parse command line arguments
int main(int argc, char **argv) {
    // Setup defaults
    const char *server = "sentinel.turris.cz";
    const char *cert = "/var/run/dynfw/server.cert";
    dump_path = NULL;
    int port = 7087;

    // Parse options
    struct option long_options[] = {
        {"server",  required_argument, 0,  's'},
        {"port",    required_argument, 0,  'p'},
        {"cert",    required_argument, 0,  'c'},
        {"output",  required_argument, 0,  'o'},
        {"verbose", no_argument,       0,  'v'},
        {"help",    no_argument,       0,  'h'},
        {0,0,0,0}
    };
    int c;
    int option_index = 0;
    while((c = getopt_long(argc, argv, "s:o:p:c:i:vh", long_options, &option_index)) != -1) {
        switch(c) {
            case 's':
                server = optarg;
                break;
            case 'c':
                cert = optarg;
                break;
            case 'o':
                dump_path = strdup(optarg);
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                printf("Setting verbose mode %d\n", verbose);
                break;
            case 'h':
                printf(
                    "Usage: %s [OPTION...]\n\n"
                    "   -s, --server=SERVER     server to connect to (default %s)\n"
                    "   -p, --port=PORT         port on the server to connect to (default %d)\n"
                    "   -c, --cert=CERT         path to the server certificate (default %s)\n"
                    "   -o, --output=FILE       path to the output file (default %s)\n"
                    "   -v, --verbose           turn on verbose output\n"
                    "   -h, --help              show help message and exit\n\n"
                    , argv[0], server, port, cert, dump_path
                );
                exit(0);
        };
    }

    char buff[1024];
    snprintf(buff, 1023, "tcp://%s:%d", server, port);
    encrypted_loop(buff, cert);
    return 0;
}
