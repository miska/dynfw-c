#include <set>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>

#include "set_manager.h"
#include "utils.h"

int set_manager::add(const char *ip) {
    bad_guy_manager::add(ip);
    db.insert(std::string(ip));
    return 0;
}

int set_manager::remove(const char *ip) {
    bad_guy_manager::remove(ip);
    db.erase(ip);
    return 0;
}

int set_manager::clear() {
    bad_guy_manager::clear();
    db.clear();
    return 0;
}

set_manager::set_manager(const char *persistent) {
    if(persistent == NULL) {
        cache = NULL;
    } else {
        cache = strdup(persistent);
        std::ifstream cache_fl(cache);
        std::string line;
        if(cache_fl.is_open()) {
            while(std::getline(cache_fl, line)) {
                add(line.c_str());
            }
        }
        cache_fl.close();
    }
}

int set_manager::snapshot(const char *path) {
    std::ofstream cache_fl;
    if(verbose)
        zsys_info("Creating snapshot of %d bad guys to %s", db.size(),path);
    cache_fl.open(path, std::ios::out | std::ios::trunc);
    if(cache_fl.is_open()) {
        for(auto ip:db) {
            cache_fl << ip << '\n';
        }
        cache_fl.close();
    } else {
        zsys_info("Can't open file %s", path);
    }
    return 0;
}

set_manager::~set_manager() {
    if(cache != NULL) {
        snapshot(cache);
        free(cache);
    }
}
