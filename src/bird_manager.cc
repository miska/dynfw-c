#include <set>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>

#include "bird_manager.h"
#include "utils.h"

int bird_manager::add(const char *ip) {
    bad_guy_manager::add(ip);
    changes++;
    db.insert(std::string(ip));
    snapshot();
    return 0;
}

int bird_manager::remove(const char *ip) {
    bad_guy_manager::remove(ip);
    changes++;
    db.erase(ip);
    snapshot();
    return 0;
}

int bird_manager::clear() {
    changes+=db.size();
    bad_guy_manager::clear();
    db.clear();
    return 0;
}

bird_manager::bird_manager(time_t r_time, int changes, const char *table, const char* tmp_path) {
    this->tmp_path = tmp_path;
    this->m_changes = changes;
    this->r_time = r_time;
    this->table = table;
}

int bird_manager::snapshot(bool force) {
    std::ofstream cache_fl;
    // Skip refresh on first load
    if(last_change == 0 && !force) {
        last_change = time(NULL);
        return 0;
    }
    if((time(NULL) - last_change < r_time) && !force) {
        if(verbose)
            zsys_info("Too soon to update");
        return 0;
    }
    if((changes < m_changes) && !force) {
        if(verbose)
            zsys_info("Not enough changes yet");
        return 0;
    }
    if(verbose)
        zsys_info("Creating snapshot of %d bad guys to %s (name %s)", db.size(), tmp_path.c_str(), table.c_str());
    cache_fl.open(tmp_path, std::ios::out | std::ios::trunc);
    if(cache_fl.is_open()) {
        cache_fl << std::string("") <<
                    "flow4 table " << table << "_tab;\n"
                    "protocol static " << table << "_prot {\n"
                    "    flow4;\n"
                    ;
        for(auto ip:db) {
            cache_fl << std::string("") <<
                    "    route flow4 {\n" <<
                    "        src " << ip << "/32;\n";
            cache_fl << std::string("") <<
                    "    } {\n"
                    "        bgp_ext_community.add( (generic, 0x80060000, 0x0 ) );\n"
                    "    };\n";
        }
        cache_fl << "}\n";
        cache_fl.close();
        int ret = systemf("birdcl configure", tmp_path.c_str());
        if(ret == 0 ) {
            last_change = time(NULL);
            changes = 0;
        } else {
            zsys_error("Can't reload bird!");
        }
        return ret;
    } else {
        zsys_error("Can't open file %s", tmp_path.c_str());
        return 1;
    }
}

bird_manager::~bird_manager() {
}
