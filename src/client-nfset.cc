/**
 * \file client-nfset.cc
 *
 * \brief DynFW CLI program to manage a designated nftables set
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <czmq.h>
#include <zsys.h>
#include <msgpack.h>
#include <getopt.h>

#include "client.h"
#include "utils.h"
#include "nfset_manager.h"

const char *table = "turris";
const char *set = "dynfw";

/**
 * \brief Main loop iterating over the messages
 *
 * Basically just construct `client` object and `nfset_manager` object and asks
 * client to process every message that comes through.
 *
 * @see client#process_message
 *
 */
void encrypted_loop(const char *endpoint, const char *certpath) {
    zsys_info("Connecting to %s using %s", endpoint, certpath);

    nfset_manager ip(table, set);
    client cl(endpoint,certpath);

    //  Main loop
    while(zsys_interrupted == 0) {
        cl.process_message(&ip);
    }
}

//! Main body to parse command line arguments
int main(int argc, char **argv) {
    // Setup defaults
    const char *server = "sentinel.turris.cz";
    const char *cert = "/var/run/dynfw/server.cert";
    int port = 7087;

    // Parse options
    struct option long_options[] = {
        {"server",  required_argument, 0,  's'},
        {"port",    required_argument, 0,  'p'},
        {"cert",    required_argument, 0,  'c'},
        {"set",     required_argument, 0,  'i'},
        {"table",   required_argument, 0,  't'},
        {"verbose", no_argument,       0,  'v'},
        {"help",    no_argument,       0,  'h'},
        {0,0,0,0}
    };
    int c;
    int option_index = 0;
    while((c = getopt_long(argc, argv, "s:p:c:i:vh", long_options, &option_index)) != -1) {
        switch(c) {
            case 's':
                server = optarg;
                break;
            case 'c':
                cert = optarg;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'i':
                set = optarg;
                break;
            case 't':
                table = optarg;
                break;
            case 'v':
                verbose = true;
                printf("Setting verbose mode %d\n", verbose);
                break;
            case 'h':
                printf(
                    "Usage: %s [OPTION...]\n\n"
                    "   -s, --server=SERVER     server to connect to (default %s)\n"
                    "   -p, --port=PORT         port on the server to connect to (default %d)\n"
                    "   -c, --cert=CERT         path to the server certificate (default %s)\n"
                    "   -i, --set=SET           name of the ip set in nftables (default %s)\n"
                    "   -t, --table=TABLE       name of the nftables table (default %s)\n"
                    "   -v, --verbose           turn on verbose output\n"
                    "   -h, --help              show help message and exit\n\n"
                    , argv[0], server, port, cert, set, table
                );
                exit(0);
        };
    }

    char buff[1024];
    snprintf(buff, 1023, "tcp://%s:%d", server, port);
    encrypted_loop(buff, cert);
    return 0;
}
