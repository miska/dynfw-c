#include "bad_guy_manager.h"
#include "utils.h"

int bad_guy_manager::clear() {
    if(verbose)
        zsys_info("Clearing list of bad guys");
    return 0;
}

int bad_guy_manager::add(const char *ip) {
    if(verbose)
        zsys_info("Adding bad guy with IP %s", ip);
    return 0;
}

int bad_guy_manager::remove(const char *ip) {
    if(verbose)
        zsys_info("Removing former bad guy with IP %s", ip);
    return 0;
}

